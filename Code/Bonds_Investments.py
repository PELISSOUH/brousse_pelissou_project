"""
NAME: BROUSSE Anais and PELISSOU Hugo

This part will model a Bonds, knowing that there are Short-Term (ST) and Long-Term (LT) Bonds.
We have constructed the code in order to automatically set the yield of the two types of Bonds (1.5 % for a ST and 3%
for a LT).
"""

# ----- Package ----- #
import numpy as np


# ----- Code ----- #

class Bonds(object):
    def __init__(self, notional, term):
        self.notional = notional
        self.term = term

    def give_PnL_year(self):
        interest = [(((1 + self.Bonds_Rate()) ** i - 1) * self.notional) for i in np.arange(1, int((self.term / 365) + 1))]
        return interest

    def give_PnL(self):
        # Construction of the compounded interest at maturity, according to the chosen investment, annually
        if self.Bonds_Rate() != 0:
            interest = [((1 + self.Bonds_Rate()) ** i - 1) for i in np.arange(1, (self.term / 365) + 1)]
            # And we will now calculate the final PnL, who are simply the last compounded interest * the notional:
            PnL = interest[-1] * self.notional
        else:
            # By error, we decide to send 0 (because no gain on the investment)
            PnL = 0
        return round(PnL, 2)

    def Bonds_Rate(self):
        return False


class ShortTermBonds(Bonds):
    def Bonds_Rate(self):
        # We define the conditions of a short term bond
        if (self.term / 365) >= 2 and self.notional >= 250:
            r = float(0.015)
        else:
            # If the conditions are not verified, we simply return his initial budget
            r = float(0)
        return r


class LongTermBonds(Bonds):
    def Bonds_Rate(self):
        # We define the conditions of a long term bond
        if (self.term / 365) >= 5 and self.notional >= 1000:
            r = float(0.03)
        else:
            # If the conditions are not verified, we simply return his initial budget
            r = float(0)
        return r
