# ----- Part2 - Overview----- #
from Stocks_Investments import *
import matplotlib.pyplot as plt


# ----- #Plot of all stocks for the entire period ----- #
plt.figure(figsize=(15, 7))
plt.plot(FDX['Date'], FDX['high'], label='FDX')
plt.plot(GOOGL['Date'], GOOGL['high'], label='Google')
plt.plot(IBM['Date'], IBM['high'], label='IBM')
plt.plot(KO['Date'], KO['high'], label='KO')
plt.plot(MS['Date'], MS['high'], label='MS')
plt.plot(NOK['Date'], NOK['high'], label='NOK')
plt.plot(XOM['Date'], XOM['high'], label='XOM')
plt.title("Stock representation of the entire periods")
plt.xlabel('Time')
plt.ylabel('Stock Price')
plt.ylim(0, 1860)
plt.legend()
plt.grid()
plt.show()


# --- Result Overview --- #

# Entire stock
ST1 = Stocks(1582, 100, FDX, '2016-09-01')
print("The return of your investment is " + str(round(ST1.give_return() * 100, 2)) + "%.")
print("Your profit is of " + str(round(ST1.give_profit(), 2)) + "$.")
print(ST1.give_number_Stocks())


# Example with Christmas date
ST2 = Stocks(1467, 100, FDX, '2016-12-25')
print(ST2.True_Buy_Date())
print("The return of your investment is " + str(round(ST2.give_return() * 100, 2)) + "%.")
print("Your profit is of " + str(round(ST2.give_profit(), 2)) + "$.")


# Problem with to big date range (sell date)
ST3 = Stocks(2000, 100, FDX, '2016-09-01')
print("The return of your investment is " + str(round(ST3.give_return(), 2)) + ".")
print("The Sell Date is " + str(ST3.True_Sell_Date()) + ".")
