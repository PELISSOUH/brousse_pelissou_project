"""
The third part will have for objective to model 3 types of investors:

- A defensive investor will only invest in short and long term bonds. He will randomly invest (50-50) in long and
short term bonds  as long as he has at least enough money to pay for a short term bond.

- An aggressive investor will only invest in stocks. First a randomly chosen stock will be selected.
Then, depending on the investor remaining budget, a random amount of stocks between 0 and the maximum of stocks that
investor would be able to buy is bought. This is repeated until he has less than 100$ available.

- A mixed investor will invest first have a 25-75 chance of buying a bond or stocks. If he buys a bond it is
distributed 50-50 in long and short. If he buys a stock, the same rules apply as the aggressive investor.
In this case the mixed investor will keep on investing until the has not enough to buy the short term bond.
"""

# ----- Package ----- #

from random import *
from Stocks_Investments import *
from Bonds_Investments import LongTermBonds, ShortTermBonds


# ----- Code ----- #

# First of all we will define a vector of each stocks:
stocks = {"FDX": FDX, "GOOGL": GOOGL, "XOM": XOM, "KO": KO, "NOK": NOK, "MS": MS, "IBM": IBM}


# We will define a class investor and 3 subclasses for each types of investors:

class Investors(object):
    def __init__(self, date, term, budget):
        self.date = date
        self.term = term
        self.budget = budget


class Defensive(Investors):
    def give_gain(self):
        n = random()
        if n < 0.5:
            simul = ShortTermBonds(self.budget, self.term)
            ret = simul.give_PnL()
        else:
            simul = LongTermBonds(self.budget, self.term)
            ret = simul.give_PnL()
        return ret


class Aggressive(Investors):
    def give_gain(self):
        ret = 0
        while self.budget >= 100:
            stocks_rand = choice(list(stocks))
            budget_rand = uniform(0, self.budget)
            self.budget = self.budget - budget_rand
            shares = Stocks(self.term, self.budget, stocks[stocks_rand], self.date)
            ret = ret + shares.give_return()
            return round(ret * float(self.budget), 2)


class Mixed(Investors):
    def give_gain(self):
        n = random()
        if n < 0.25:
            stocks_return = Defensive(self.date, self.term, self.budget).give_gain()
        else:
            stocks_return = Aggressive(self.date, self.term, self.budget).give_gain()
        return stocks_return
