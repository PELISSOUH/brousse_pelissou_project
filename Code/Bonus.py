"""
We will try to answers at the 3 following bonus questions:
"""
# ----- Package ----- #

import numpy as np
from Inverstors import *
from Stocks_Investments import *
import matplotlib.pyplot as plt


"""
1) Model the same number of investors but now have starting budget randomly from a normal distribution centered around
20000 with a standard deviation of 5000 (keep in mind the tails to the lower side).
"""

# The code will be as in the "Investors" part, but the only difference will be the simulation of the "self.Budget"

customersDefensive = []
customersAggressive = []
customersMixed = []

for i in range(0, 1500):
    if i < 500:
        customersDefensive.append(Defensive("2016-09-01", randint(1, 20000), np.random.normal(20000, 5000)).give_gain())
    elif i < 1000:
        customersAggressive.append(Aggressive("2016-09-01", 1582, np.random.normal(20000, 5000)).give_gain())
    else:
        customersMixed.append(Mixed("2016-09-01", 1582, np.random.normal(20000, 5000)).give_gain())

print(customersDefensive)
print(customersAggressive)
print(customersMixed)

plt.boxplot([customersDefensive, customersMixed, customersMixed], notch=True,
            labels=["Defensive_Customers", "Aggressive_Customers", "Mixed_Customers"], showfliers=False)
plt.show()


"""
2) What was the best stock to have in 2017?
"""
# To answers this questions we will use the Stocks classes

S_Fdx = Stocks(360, 100, FDX, "2017-01-03")
S_GOOGL = Stocks(360, 100, GOOGL, "2017-01-03")
S_XOM = Stocks(360, 100, XOM, "2017-01-03")
S_KO = Stocks(360, 100, KO, "2017-01-03")
S_NOK = Stocks(360, 100, NOK, "2017-01-03")
S_MS = Stocks(360, 100, MS, "2017-01-03")
S_IBM = Stocks(360, 100, IBM, "2017-01-03")

print("In 2017, the stocks value (in pourcent) for : FDX is %s - GOOGL is %s - XOM is %s - KO is %s - NOK is %s - "
      "MS is %s - IBM is %s." % (S_Fdx.give_return(), S_GOOGL.give_return(), S_XOM.give_return(), S_KO.give_return(),
                                 S_NOK.give_return(), S_MS.give_return(), S_IBM.give_return()))

Max_Stcoks = [S_Fdx.give_return(), S_GOOGL.give_return(), S_XOM.give_return(), S_KO.give_return(), S_NOK.give_return(),
              S_MS.give_return(), S_IBM.give_return()]
print("The maximum return around the stocks in 2017 are " + str(max(Max_Stcoks)) + ". This correspond to FDX.")
