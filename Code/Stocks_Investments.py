"""
This second part will permit to model a Stock.
It will take into account the following listed companies : 'FDX', 'GOOGL','XOM', 'KO', 'NOK', 'MS', 'IBM‘.
We will build functions to display the return of the stock, the investor's profit, as well as the number of
shares purchased.
"""

# ----- Package and Data----- #
import pandas as pd
from datetime import timedelta


# Import all databases, with only the necessary column "high".
FDX = pd.read_csv('../Data/FDX_semicolon.csv', sep=";", usecols=['high'])
GOOGL = pd.read_csv('../Data/GOOGL_semicolon.csv', sep=";", usecols=['high'])
IBM = pd.read_csv('../Data/IBM_semicolon.csv', sep=";", usecols=['high'])
KO = pd.read_csv('../Data/KO_semicolon.csv', sep=";", usecols=['high'])
MS = pd.read_csv('../Data/MS_semicolon.csv', sep=";", usecols=['high'])
NOK = pd.read_csv('../Data/NOK_semicolon.csv', sep=";", usecols=['high'])
XOM = pd.read_csv('../Data/XOM_semicolon.csv', sep=";", usecols=['high'])


# Add the date to the database. You can see on the report why we do the choice to use this method.

tradingday = pd.read_csv('../Data/ustradingday.csv', sep=";", usecols=['Date'])
tradingday['Date'] = pd.to_datetime(tradingday['Date'], format="%Y-%m-%d")


FDX['Date'] = tradingday
GOOGL['Date'] = tradingday
IBM['Date'] = tradingday
KO['Date'] = tradingday
MS['Date'] = tradingday
NOK['Date'] = tradingday
XOM['Date'] = tradingday


# ----- Code ----- #

class Stocks(object):
    def __init__(self, term, notional, stockname, date):
        self.term = term
        self.notional = notional
        self.stockname = stockname
        self.date = pd.to_datetime(date, format="%Y-%m-%d")
        self.BuyDate = self.date
        self.SellDate = self.date + timedelta(days=self.term)

    def True_Buy_Date(self):
        # In case the Buy Date is not a business day, we will assign the closest one.
        while self.BuyDate not in set(self.stockname['Date']):
            self.BuyDate = self.BuyDate - timedelta(days=1)
        return self.BuyDate

    def True_Sell_Date(self):
        # In case the Sell Date is not a business day, we will assign the closest one.
        while self.SellDate not in set(self.stockname['Date']):
            self.SellDate = self.SellDate - timedelta(days=1)
        return self.SellDate

    def give_return(self):
        # First we will allocate the purchase price (P_O) and then the sale price (P_N).
        P_0 = float(self.stockname.loc[self.stockname['Date'] == self.True_Buy_Date(), 'high'])
        P_N = float(self.stockname.loc[self.stockname['Date'] == self.True_Sell_Date(), 'high'])
        # We will now calculate the return with the price differences
        Stock_return = (P_N - P_0) / P_0
        return Stock_return

    def return_yearly(self):
        # We calculate now the yearly return for the needed stocks
        self.stockname['daily_return'] = self.stockname["high"].pct_change(1) * self.notional
        return self.stockname.groupby(pd.Grouper(key='Date', freq='Y')).sum()['daily_return']

    def give_profit(self):
        profit = self.notional * self.give_return()
        return profit

    def give_number_Stocks(self):
        nb = self.stockname.loc[self.stockname['Date'] == self.BuyDate, 'high']
        return nb
