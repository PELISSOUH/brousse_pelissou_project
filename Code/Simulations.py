"""
This last part will permit to model the following scénarios:

1) By taking the same input parameters as above (part3) and again modelling the same number of investors with that
starting capital, plot the means for every year in the given dataset (always going from 01/01 to 31/12)

2) From now on, mixed investors redistribute their budget randomly again when their bond periods end
(again 25-75 bonds vs stocks, 50-50 long term short, …) what is the impact? What if we switch 75-25 bonds vs stocks
Do you notice a change when multiplying all starting budgets by 10?
"""

# ----- Package ----- #
from random import *
from Stocks_Investments import *
from Bonds_Investments import *
import matplotlib.pyplot as plt


# ----- Subpart 1 - Annually return ----- #
""" To answer this first question, we had built part 3 in such a way that we didn't have to make big changes for
 this last part. So you can see that this code is almost identical to the previous one. """

stocks = {"FDX": FDX, "GOOGL": GOOGL, "XOM": XOM, "KO": KO, "NOK": NOK, "MS": MS, "IBM": IBM}


class Investors(object):
    def __init__(self, date, term, budget):
        self.date = date
        self.term = term
        self.budget = budget


class Defensive(Investors):
    def give_gain_by_year(self):
        n = random()
        if n < 0.5:
            simul = ShortTermBonds(self.budget, self.term)
            ret = simul.give_PnL_year()
        else:
            simul = LongTermBonds(self.budget, self.term)
            ret = simul.give_PnL_year()
        return ret


class Aggressive(Investors):
    def give_gain_by_year(self):
        ret = 0
        while self.budget >= 100:
            stocks_rand = choice(list(stocks))
            budget_rand = uniform(0, self.budget)
            self.budget = self.budget - budget_rand
            shares = Stocks(self.term, self.budget, stocks[stocks_rand], self.date)
            ret += shares.return_yearly()
        return ret


class Mixed(Investors):
    def give_gain_by_year(self):
        n = random()
        if n < 0.75:
            stocks_return = Aggressive(self.date, self.term, self.budget).give_gain_by_year()
        else:
            stocks_return = Defensive(self.date, self.term, self.budget).give_gain_by_year()
        return stocks_return


# ----- Simulation of the 1500 investors with the return yearly and plotting the timeseries ----- #

date = ['2016-12-31', '2017-12-31', '2018-12-31', '2019-12-31', '2020-12-31']
Agg_Df = pd.DataFrame(columns=date)
Def_Df = pd.DataFrame(columns=date)
Mix_Df = pd.DataFrame(columns=date)

for i in range(1500):
    if i < 500:
        Agg = Aggressive("2016-09-01", 1825, 5000)
        Agg_Df[i] = Agg.give_gain_by_year()
    elif 500 < i <= 1000:
        Def = Defensive("2016-09-01", 1825, 5000)
        Def_Df[i] = Def.give_gain_by_year()
    else:
        Mix = Mixed("2016-09-01", 1825, 5000)
        Mix_Df[i] = Mix.give_gain_by_year()


# Plot :
plt.figure(figsize=(15, 6))
plt.plot(date, Agg_Df.mean(axis=1), label='Aggressive Investors', color="r")
plt.plot(date, Def_Df.mean(axis=1), label='Defensive Investors', color="b")
plt.plot(date, Mix_Df.mean(axis=1), label='Mixed Investors ', color="g")
plt.title("Timeseries representation of the 3 types of investors")
plt.xlabel('Date')
plt.ylabel('Return')
plt.legend(loc='upper left')
plt.show()


# ----- Subpart 2 - Change of Value ----- #

Agg2_Df = pd.DataFrame(columns=date)
Def2_Df = pd.DataFrame(columns=date)
Mix2_Df = pd.DataFrame(columns=date)

for i in range(1500):
    if i < 500:
        Agg = Aggressive("2016-09-01", 1825, randint(1, 5000))
        Agg2_Df[i] = Agg.give_gain_by_year()
    elif 500 < i <= 1000:
        Def = Defensive("2016-09-01", 1825, randint(1, 5000))
        Def2_Df[i] = Def.give_gain_by_year()
    else:
        Mix = Mixed("2016-09-01", 1825, randint(1, 5000))
        Mix2_Df[i] = Mix.give_gain_by_year()

plt.figure(figsize=(15, 6))
plt.plot(date, Agg2_Df.mean(axis=1), label='Aggressive Investors', color="r")
plt.plot(date, Def2_Df.mean(axis=1), label='Defensive Investors', color="b")
plt.plot(date, Mix2_Df.mean(axis=1), label='Mixed Investors ', color="g")
plt.title("Timeseries representation of the 3 types of investors for a random Budget between 1 and 5000")
plt.xlabel('Date')
plt.ylabel('Return')
plt.legend(loc='upper left')
plt.show()


# For the 75/25 in Bonds/Stocks, we only change the mixed class as follow:
"""
class mixed(Investors):
    def give_gain_by_year(self):
        n = random()
        if n < 0.25:
            Stocks_return = aggressive(self.Date, self.Term, self.Budget).give_gain_by_year()
        else:
            Stocks_return = defensive(self.Date, self.Term, self.Budget).give_gain_by_year()
        return Stocks_return
"""
# And we get the graph call "Timeseries with a weight of 75% in the bonds class for the mixed investors_Part4" in the Results documents.


"""
Multyplying the budget (5000) by 10 will not have an impact on the percentage of return but just on the total return.
Our graph represent the gain do by the investors in percent. So the budget will not have a change in the shape of the
curve.
 """