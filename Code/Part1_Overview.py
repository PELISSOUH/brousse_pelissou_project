# ----- Part 1 - Overview ----- #

from Bonds_Investments import *
import matplotlib.pyplot as plt

STBond = ShortTermBonds(250, 18250)
LTBond = LongTermBonds(1000, 18250)

# 1) Compute the compounded interest at maturity
print("The profit in the Bonds at Maturity is $" + str(round((STBond.give_PnL()), 2)) + ".")
print("The profit in the Bonds at Maturity is $" + str(round((LTBond.give_PnL()), 2)) + ".")


# 2) Make a plot of the evolution of the investment of the minimum allowed amount over a period of 50 years.
priceST = [(STBond.notional * (1 + STBond.Bonds_Rate()) ** i - 1) for i in range(1, 51)]
priceLT = [(LTBond.notional * (1 + LTBond.Bonds_Rate()) ** i - 1) for i in range(1, 51)]

plt.figure(figsize=(15, 6))  # This will display a larger figure.
plt.plot(range(1, 51), priceST, label='Short Term', color="r")
plt.plot(range(1, 51), priceLT, label='Long Term', color="b")
plt.title("Investment evolution over a period of 50 years for the two types of Bonds and the minimum amount")
plt.xlabel('Maturity')
plt.ylabel('Investment')
plt.xlim(0, 52)
plt.ylim(0, 4600)
plt.legend(loc='upper left')
plt.show()
