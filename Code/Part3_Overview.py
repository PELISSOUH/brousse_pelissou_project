# ----- Part 3 - Overview ----- #
from Inverstors import *
import matplotlib.pyplot as plt


# Defensive test:
deffens = Defensive("2016-09-01", 1582, 5000)
print("The gain for the Defensive investor is $" + str(deffens.give_gain()) + ".")

# Aggressive test:
aggres = Aggressive("2016-09-01", 1582, 5000)
print("The gain for the Aggressive investor is $" + str(aggres.give_gain()) + ".")

# Mixed test:
mix = Mixed("2016-09-01", 1582, 5000)
print("The gain for the Mixed investor is $" + str(mix.give_gain()) + ".")


# ----- Simulation for 1500 customers, where 500 are Defensives, 500 are Aggressive and 500 mixed ----- #
customersDefensive = []
customersAggressive = []
customersMixed = []


for i in range(0, 500):
    customersDefensive.append(Defensive("2016-09-01", randint(1, 2000), 5000).give_gain())
    customersAggressive.append(Aggressive("2016-09-01", randint(1, 2000), 5000).give_gain())
    customersMixed.append(Mixed("2016-09-01", randint(1, 2000), 5000).give_gain())


print(customersDefensive)
print(customersAggressive)
print(customersMixed)


# For a better view, let represent each customer type with a boxplot.
plt.boxplot([customersDefensive, customersMixed, customersMixed], notch=True,
            labels=["Defensive_Customers", "Aggressive_Customers", "Mixed_Customers"], showfliers=False)
plt.show()
